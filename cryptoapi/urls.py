from django.urls import path
from cryptoapi.views import RequestView,GetJsonView
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('request/', RequestView.as_view()),
    path('get_json/',GetJsonView.as_view()),
]
urlpatterns = format_suffix_patterns(urlpatterns)