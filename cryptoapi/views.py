from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from pubapidemo.api_requester import RequestController
from .serializers import CryptoDataSerializer
from .models import Tickers
from django.http import JsonResponse , HttpResponse
from pubapidemo.constants import Constants
from django.template import loader

class RequestView(APIView):
    def get(self, request, format=None):
        controller = RequestController("https://api.coinmarketcap.com/v2/ticker/")
        controller.process()
        return Response("data fetched")


class GetJsonView(APIView):
    def get(self, request):
        tickers = Tickers.objects.all().order_by('-' + Constants.CONST_FIELD_TSTAMP, Constants.CONST_TAB_FIELD_RANK)[:50]
        dashboard_template = loader.get_template("cryptoapi/dashboard.html")
        context = {
            'tickers': tickers
        }
        return HttpResponse(dashboard_template.render(context,request))
