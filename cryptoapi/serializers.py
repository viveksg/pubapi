from rest_framework import serializers
from pubapidemo.constants import Constants
from cryptoapi.models import Tickers


class CryptoDataSerializer(serializers.Serializer):
    cid = serializers.IntegerField()
    name = serializers.CharField(max_length=100)
    symbol = serializers.CharField(max_length=100)
    rank = serializers.IntegerField()
    timestamp = serializers.IntegerField()
    last_updated = serializers.IntegerField()
    total_supply = serializers.IntegerField()
    price = serializers.FloatField()

    def create(self, validated_data):
        return Tickers.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.cid = validated_data.get(Constants.CONST_ID, instance.cid)
        instance.name = validated_data.get(Constants.CONST_TAB_FIELD_NAME, instance.name)
        instance.symbol = validated_data.get(Constants.CONST_TAB_FIELD_SYMBOL, instance.symbol)
        instance.rank = validated_data.get(Constants.CONST_TAB_FIELD_RANK, instance.rank)
        instance.timestamp = validated_data.get(Constants.CONST_FIELD_TSTAMP, instance.timestamp)
        instance.last_updated = validated_data.get(Constants.CONST_TAB_FIELD_UPD, instance.last_updated)
        instance.total_supply = validated_data.get(Constants.CONST_TAB_FIELD_SUPPLY, instance.total_supply)
        instance.price = validated_data.get(Constants.CONST_TAB_FIELD_PRICE, instance.price)
        instance.save()
        return instance
