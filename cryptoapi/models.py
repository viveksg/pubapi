from django.db import models

class Tickers(models.Model):
    cid = models.IntegerField()
    name = models.TextField()
    symbol = models.TextField()
    rank = models.IntegerField()
    timestamp = models.BigIntegerField()
    last_updated = models.BigIntegerField()
    total_supply = models.BigIntegerField()
    price = models.FloatField()

    class Meta:
        ordering = ('last_updated',)
