Requirement: Docker with docker-cmpose tool  


Usage:  
Go in project directory and type following commands  
    sudo docker-compose build  
    sudo docker-compose up  
     

Check terminal, once server is started make following request       
http://127.0.0.1:8000/request  
This will fill up db with API data. See terminal for progress 
 
Then make request to http://127.0.0.1:8000/get_json  
This will show tabular data
     