FROM ubuntu:latest

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y python3-pip build-essential libssl-dev libffi-dev python-dev
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y --fix-missing libmysqlclient-dev

COPY . /pubapi
WORKDIR /pubapi

EXPOSE 8000 3306 33060 3308

RUN pip3 install --upgrade setuptools
RUN pip3 install -r requirements.txt


