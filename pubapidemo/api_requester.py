import urllib.request
import json
from .constants import Constants
import time
import calendar
from cryptoapi.models import Tickers
from cryptoapi.serializers import CryptoDataSerializer
from django.db import transaction


class APIRequester:
    def __init__(self, url):
        self.url = url

    def getData(self):
        with urllib.request.urlopen(self.url) as request:
            return request.read()

    def updateURL(self, url):
        self.url = url


class RequestController:
    def __init__(self, base_url):
        self.url = base_url
        self.requester = None

    def process(self):
        initial = 1
        max = 2
        tstamp = calendar.timegm(time.gmtime())
        records = []
        while max > initial:
            print(str(initial) + " " + str(max))
            full_url = self.url + "?" + Constants.CONST_START + "=" + str(
                initial) + "&" + Constants.CONST_SORT + "=" + Constants.CONST_ID
            print(full_url)
            if self.requester is None:
                self.requester = APIRequester(full_url)
            else:
                self.requester.updateURL(full_url)
            data = self.requester.getData()
            if data is not None:
                json_data = json.loads(data);
                values = json_data[Constants.CONST_FIELD_DATA]
                metadata = json_data[Constants.CONST_FIELD_METADATA]
                max = metadata[Constants.CONST_FIELD_NUM]

                for dvalue in values:
                    value = values[dvalue]
                    ticker_data = self.getTicker(value, tstamp, Constants.FIELD_LIST)
                    records.append(ticker_data)
                initial += len(values)
            print(str(initial) + " " + str(max))

        if len(records) > 0:
            self.saveRecords(records)
            records.clear()

    @transaction.atomic
    def saveRecords(self, records):
        for record in records:
            serializer = CryptoDataSerializer(data=record)
            if serializer.is_valid():
                serializer.save()

    @staticmethod
    def getTicker(json_values, timestamp, fields):
        data = dict()
        for field in fields:
            if field == Constants.CONST_FIELD_TSTAMP:
                data[field] = timestamp
            elif field == Constants.CONST_ID:
                data[field] = json_values[Constants.CONST_TAB_FIELD_ID]
            elif field == Constants.CONST_TAB_FIELD_PRICE:
                data[field] = json_values[Constants.CONST_FIELD_QUOTES][Constants.CONST_FIELD_USD][
                    Constants.CONST_TAB_FIELD_PRICE]
            else:
                data[field] = json_values[field]
        return data
