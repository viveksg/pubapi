class Constants:
    CONST_START = "start"
    CONST_SORT = "sort"
    CONST_ID = "cid"
    CONST_FIELD_DATA = "data"
    CONST_FIELD_METADATA = "metadata"
    CONST_FIELD_NUM = "num_cryptocurrencies"
    CONST_FIELD_TSTAMP = "timestamp"
    CONST_TAB_NAME = "cryptodata"
    CONST_TAB_FIELD_ID = "id"
    CONST_TAB_FIELD_NAME = "name"
    CONST_TAB_FIELD_SYMBOL= "symbol"
    CONST_TAB_FIELD_RANK = "rank"
    CONST_TAB_FIELD_UPD = "last_updated"
    CONST_TAB_FIELD_SUPPLY = "total_supply"
    CONST_TAB_FIELD_PRICE = "price"
    CONST_FIELD_QUOTES = "quotes"
    CONST_FIELD_USD  = "USD"
    FIELD_LIST = [CONST_ID,
                  CONST_TAB_FIELD_NAME,
                  CONST_TAB_FIELD_SYMBOL,
                  CONST_TAB_FIELD_RANK,
                  CONST_FIELD_TSTAMP,
                  CONST_TAB_FIELD_UPD,
                  CONST_TAB_FIELD_SUPPLY,
                  CONST_TAB_FIELD_PRICE
                  ]
